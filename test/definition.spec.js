const i = require('../../lib/interpreter.js');

describe('definision', () => {
  it('should def constants', () => {
    expect(() => {
      try {
        i.exec('(def x 10)')
      } catch (err) {
        console.log(err.message);
      }
    }).not.toThrow();
  });


  it('should def a list by using a backquote', () => {
    const result = i.exec('`("a" "b" "c")');
    expect(result).toEqual(["a","b","c"]);
  });


  it('should return a value', () => {
    try {
      const result = i.exec('(def x 10)\n(x)');

      expect(result).toBe(10);
    } catch (err) {
      console.log(err.message);
    }
  });

  it('should defind a string and then return it', () => {
    const result = i.exec('(def hello "hello world")(hello)');

    expect(result).toBe('hello world');
  });

  it('should exec more than one definition', () => {
    const result = i.exec('(def foo "hello world")\n\n(def bar 10)\n\n\n(bar)');
    expect(result).toBe(10);
  });


    it('should def a function with two params', () => {
      const result = i.exec('(def (sum a b) (+ a b))(sum 2 3)')

      expect(result).toBe(5);
    });





    it('should define a lambda with two or more args', () => {
      const result = i.exec(
        `
        (def test ($_ (a b) (+ a b)))
        (test 10 20)
        `
      );

      expect(result).toBe(30);
    });



    it('should return result of a last expression', () => {
      const result = i.exec(`
                          (let ((x 2) (y 3))
                              (let ((foo ($_ (z) (+ x y z)))
                                    (x 7))
                                (foo 4)))
                            `);

      expect(result).toBe(14);
    });



  describe('set', () => {
    it('should update a value in a variable', () => {
      const result = i.exec('(def a 10)\n(set! a 20)\n(a)');

      expect(result).toBe(20);
    });

    it('should ignore spasec', () => {
      const result1 = i.exec('(def a 10)(set! a 20)(a)');
      const result2 = i.exec('(def a 10) (set! a 20)(a)');
      const result3 = i.exec('(def a 10) (set! a 20) (a)');
      const result4 = i.exec('(def a 10)(set! a 20) (a)');

      expect(result1).toBe(20);
      expect(result2).toBe(20);
      expect(result3).toBe(20);
      expect(result4).toBe(20);
    });
  });
});

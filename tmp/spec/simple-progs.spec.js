'use strict';

var i = require('../../lib/interpreter.js');

describe('simple progs', function () {
  it('should be able to eval functions with recursion', function () {
    var result = i.exec('\n                          (def (test n)\n                            (if (< n 10)\n                              (do (set! n (+ n 1))\n                              (test n))))\n                          ');
  });

  it('should be should return a result from function with recursion', function () {
    var result = i.exec('\n(def (until pred body)\n  (if (pred)\n      (do (body)\n          (until pred body))))\n\n(def n 0)\n\n(until ($_ () (< n 10)) ($_ () (set! n (+ n 1))))\n(n)\n                          ');
    expect(result).toBe(10);
  });
});
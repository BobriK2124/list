/* parser generated by jison 0.4.17 */
/*
  Returns a Parser object of the following structure:

  Parser: {
    yy: {}
  }

  Parser.prototype: {
    yy: {},
    trace: function(),
    symbols_: {associative list: name ==> number},
    terminals_: {associative list: number ==> name},
    productions_: [...],
    performAction: function anonymous(yytext, yyleng, yylineno, yy, yystate, $$, _$),
    table: [...],
    defaultActions: {...},
    parseError: function(str, hash),
    parse: function(input),

    lexer: {
        EOF: 1,
        parseError: function(str, hash),
        setInput: function(input),
        input: function(),
        unput: function(str),
        more: function(),
        less: function(n),
        pastInput: function(),
        upcomingInput: function(),
        showPosition: function(),
        test_match: function(regex_match_array, rule_index),
        next: function(),
        lex: function(),
        begin: function(condition),
        popState: function(),
        _currentRules: function(),
        topState: function(),
        pushState: function(condition),

        options: {
            ranges: boolean           (optional: true ==> token location info will include a .range[] member)
            flex: boolean             (optional: true ==> flex-like lexing behaviour where the rules are tested exhaustively to find the longest match)
            backtrack_lexer: boolean  (optional: true ==> lexer regexes are tested in order and for each matching regex the action code is invoked; the lexer terminates the scan when a token is returned by the action code)
        },

        performAction: function(yy, yy_, $avoiding_name_collisions, YY_START),
        rules: [...],
        conditions: {associative list: name ==> set},
    }
  }


  token location info (@$, _$, etc.): {
    first_line: n,
    last_line: n,
    first_column: n,
    last_column: n,
    range: [start_number, end_number]       (where the numbers are indexes into the input string, regular zero-based)
  }


  the parseError function receives a 'hash' object with these members for lexer and parser errors: {
    text:        (matched text)
    token:       (the produced terminal token, if any)
    line:        (yylineno)
  }
  while parser (grammar) errors will also provide these members, i.e. parser errors deliver a superset of attributes: {
    loc:         (yylloc)
    expected:    (string describing the set of expected tokens)
    recoverable: (boolean: TRUE when the parser has a error recovery rule available for this particular error)
  }
*/
var parser = (function(){
var o=function(k,v,o,l){for(o=o||{},l=k.length;l--;o[k[l]]=v);return o},$V0=[1,6],$V1=[1,28],$V2=[1,18],$V3=[1,26],$V4=[1,24],$V5=[1,25],$V6=[1,27],$V7=[1,23],$V8=[1,19],$V9=[1,20],$Va=[1,21],$Vb=[1,22],$Vc=[1,7],$Vd=[34,37,38,41,43,44,46,48,54,55,57,59,61,64],$Ve=[16,17,28,29,32,33,34,37,38,39,41,43,44,46,48,54,55,57,59,61,64],$Vf=[1,77],$Vg=[1,78],$Vh=[1,70],$Vi=[1,71],$Vj=[1,74],$Vk=[1,75],$Vl=[16,17,28,29,32,33,34,37,38,39,41,43,44,46,48,54,55,57,59],$Vm=[16,17,28,29,32,33,37,38,39,41,43,44,46,48,54,55,57,59],$Vn=[1,97];
var parser = {trace: function trace() { },
yy: {},
symbols_: {"error":2,"operators":3,"+":4,"-":5,"/":6,"*":7,">":8,"<":9,"=":10,"or":11,"and":12,"not":13,"length":14,"bool":15,"true":16,"false":17,"list_func":18,"reduce":19,"nth":20,"map":21,"each":22,"push":23,"pop":24,"shift":25,"unshift":26,"value":27,"string":28,"char":29,"expr":30,"statement":31,"number":32,"name":33,"space":34,"values":35,"loop_statement":36,"loop":37,"(":38,")":39,"if_statement":40,"if":41,"let_statement":42,"local":43,"list_statement":44,"set_statement":45,"set!":46,"define_statement":47,"let":48,"id":49,"simple_expr":50,"fn_expr":51,"fn":52,"list_expr":53,"list":54,"`":55,"do_expr":56,"do":57,"print_expr":58,"print":59,"code":60,"comment":61,"program":62,"file":63,"end_of_file":64,"$accept":0,"$end":1},
terminals_: {2:"error",4:"+",5:"-",6:"/",7:"*",8:">",9:"<",10:"=",11:"or",12:"and",13:"not",14:"length",16:"true",17:"false",19:"reduce",20:"nth",21:"map",22:"each",23:"push",24:"pop",25:"shift",26:"unshift",28:"string",29:"char",32:"number",33:"name",34:"space",37:"loop",38:"(",39:")",41:"if",43:"local",44:"list_statement",46:"set!",48:"let",52:"fn",54:"list",55:"`",57:"do",59:"print",61:"comment",64:"end_of_file"},
productions_: [0,[3,1],[3,1],[3,1],[3,1],[3,1],[3,1],[3,1],[3,1],[3,1],[3,1],[3,1],[15,1],[15,1],[18,1],[18,1],[18,1],[18,1],[18,1],[18,1],[18,1],[18,1],[27,1],[27,1],[27,1],[27,1],[27,1],[27,1],[27,1],[27,2],[35,1],[35,2],[36,6],[40,7],[40,6],[42,7],[42,8],[42,8],[42,9],[42,1],[45,5],[45,6],[47,7],[47,7],[31,1],[31,1],[31,1],[31,1],[31,1],[49,1],[49,1],[49,1],[49,2],[49,2],[50,3],[50,2],[50,4],[51,7],[51,6],[51,5],[53,4],[53,5],[53,4],[56,4],[56,5],[58,5],[30,1],[30,1],[30,1],[30,1],[30,1],[60,1],[60,1],[60,1],[60,1],[62,1],[62,2],[63,2]],
performAction: function anonymous(yytext, yyleng, yylineno, yy, yystate /* action[1] */, $$ /* vstack */, _$ /* lstack */) {
/* this == yyval */

var $0 = $$.length - 1;
switch (yystate) {
case 1:
 this.$ = translator.add 
break;
case 2:
 this.$ = translator.substract 
break;
case 3:
 this.$ = translator.devide 
break;
case 4:
 this.$ = translator.multiply 
break;
case 5:
 this.$ = translator.greater 
break;
case 6:
 this.$ = translator.less 
break;
case 7:
 this.$ = translator.equal 
break;
case 8:
 this.$ = translator.or
break;
case 9:
 this.$ = translator.and
break;
case 10:
 this.$ = translator.not 
break;
case 11:
this.$ = translator.length_of
break;
case 14:
 this.$ = translator.reduce 
break;
case 15:
 this.$ = translator.nth 
break;
case 16:
 this.$ = translator.map 
break;
case 17:
 this.$ = translator.each 
break;
case 18:
 this.$ = translator.conj 
break;
case 19:
 this.$ = translator.pop 
break;
case 20:
 this.$ = translator.shift 
break;
case 21:
 this.$ = translator.unshift 
break;
case 26:
 this.$ = +$$[$0] 
break;
case 30:
 this.$ = [$$[$0]] 
break;
case 31:
 this.$ = translator.collectArgs($$[$0-1], $$[$0])
break;
case 32:
this.$ = { type: 'while', pred: $$[$0-2], body: $$[$0-1] }
break;
case 33:
 this.$ = { type: 'if', cond: $$[$0-3], true: $$[$0-2], false: $$[$0-1]} 
break;
case 34:
 this.$ = { type: 'if', cond: $$[$0-2], true: $$[$0-1] } 
break;
case 35: case 37:
 this.$ = { expr: $$[$0-3], type: 'let', values: $$[$0-1] }
break;
case 36: case 38:
 this.$ = { expr: $$[$0-4], type: 'let', values: $$[$0-1] }
break;
case 40: case 41:
 this.$ = { expr: $$[$0-2], type: 'set', values: [$$[$0-1]]  } 
break;
case 42:
 this.$ = { expr: $$[$0-3], type: 'var', values: [$$[$0-1]] }
break;
case 43:
 this.$ = { expr: $$[$0-3], type: 'function', values: $$[$0-1] }; 
break;
case 54:
 this.$ =  { id: $$[$0-1], values: [] }
break;
case 55:
 this.$ =  { values: [] }
break;
case 56:
 this.$ =  { id: $$[$0-2], values: $$[$0-1] }
break;
case 57: case 58:
 this.$ = { expr: $$[$0-3], type: 'fn', values: $$[$0-1] }
break;
case 59:
 this.$ = { expr: $$[$0-2], type: 'fn', values: $$[$0-1] }
break;
case 60: case 61: case 62:
 this.$ = `Array(${$$[$0-1].map(translator.parse)})` 
break;
case 63: case 64:
 this.$ = { type: 'do', values: $$[$0-1] }
break;
case 65:
 this.$ = { type: 'print', value: $$[$0-1] } 
break;
case 71:
 this.$ = translator.parse($$[$0])
break;
case 72:
 this.$ = translator.parse($$[$0]) 
break;
case 74:
 this.$ = '' 
break;
case 76:
 this.$ = $$[$0-1] + $$[$0] 
break;
case 77:
 return $$[$0-1] 
break;
}
},
table: [{30:4,31:5,34:$V0,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb,60:3,61:$Vc,62:2,63:1},{1:[3]},{30:4,31:5,34:$V0,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb,60:30,61:$Vc,64:[1,29]},o($Vd,[2,75]),o($Vd,[2,71]),o($Vd,[2,72]),o($Vd,[2,73]),o($Vd,[2,74]),o($Ve,[2,66]),o($Ve,[2,67]),o($Ve,[2,68]),o($Ve,[2,69]),o($Ve,[2,70]),o($Ve,[2,44]),o($Ve,[2,45]),o($Ve,[2,46]),o($Ve,[2,47]),o($Ve,[2,48]),{3:36,4:[1,46],5:[1,47],6:[1,48],7:[1,49],8:[1,50],9:[1,51],10:[1,52],11:[1,53],12:[1,54],13:[1,55],14:[1,56],18:35,19:[1,38],20:[1,39],21:[1,40],22:[1,41],23:[1,42],24:[1,43],25:[1,44],26:[1,45],33:[1,34],34:[1,37],39:[1,32],49:31,52:[1,33]},{38:[1,57]},{38:[1,58]},{38:[1,59]},{38:[1,60]},{38:[1,61]},{38:[1,62]},o($Ve,[2,39]),{38:[1,63]},{38:[1,64]},{38:[1,65]},{1:[2,77]},o($Vd,[2,76]),{15:76,16:$Vf,17:$Vg,27:69,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,34:[1,68],35:67,36:17,37:$V1,38:$V2,39:[1,66],40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},o($Ve,[2,55]),{30:80,34:[1,79],38:$V2,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},o($Vl,[2,49]),o($Vl,[2,50]),o($Vl,[2,51]),{33:[1,81]},o($Vl,[2,14]),o($Vl,[2,15]),o($Vl,[2,16]),o($Vl,[2,17]),o($Vl,[2,18]),o($Vl,[2,19]),o($Vl,[2,20]),o($Vl,[2,21]),o($Vl,[2,1]),o($Vl,[2,2]),o($Vl,[2,3]),o($Vl,[2,4]),o($Vl,[2,5]),o($Vl,[2,6]),o($Vl,[2,7]),o($Vl,[2,8]),o($Vl,[2,9]),o($Vl,[2,10]),o($Vl,[2,11]),{15:76,16:$Vf,17:$Vg,27:69,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,34:[1,83],35:82,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:69,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,35:84,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:69,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,34:[1,86],35:85,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{34:[1,87]},{34:[1,88]},{34:[1,90],38:[1,89]},{34:[1,91]},{15:76,16:$Vf,17:$Vg,27:92,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,34:[1,93],36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{34:[1,94]},o($Ve,[2,54]),{15:76,16:$Vf,17:$Vg,27:96,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,39:[1,95],40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},o($Vl,[2,53]),o($Vm,[2,30],{34:$Vn}),o($Vl,[2,22]),o($Vl,[2,23]),o($Vl,[2,24]),o($Vl,[2,25]),o($Vl,[2,26]),o($Vl,[2,27]),o($Vl,[2,28]),o($Vl,[2,12]),o($Vl,[2,13]),{30:98,38:$V2,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:69,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,34:[1,99],35:100,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},o($Vl,[2,52]),{15:76,16:$Vf,17:$Vg,27:96,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,39:[1,101],40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:69,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,35:102,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:96,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,39:[1,103],40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:96,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,39:[1,104],40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:69,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,35:105,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:106,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{30:108,33:[1,107],38:$V2,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:69,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,35:109,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{38:[1,110]},{15:76,16:$Vf,17:$Vg,27:111,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:112,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,34:$Vn,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:113,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:114,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},o($Ve,[2,56]),o($Vm,[2,31],{34:$Vn}),o($Vl,[2,29]),{34:[1,115]},{15:76,16:$Vf,17:$Vg,27:69,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,35:116,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:96,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,39:[1,117],40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},o($Ve,[2,60]),{15:76,16:$Vf,17:$Vg,27:96,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,39:[1,118],40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},o($Ve,[2,62]),o($Ve,[2,63]),{15:76,16:$Vf,17:$Vg,27:96,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,39:[1,119],40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{34:$Vn,39:[1,120]},{34:[1,121]},{34:[1,122]},{15:76,16:$Vf,17:$Vg,27:96,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,39:[1,123],40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:69,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,35:124,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:125,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,34:$Vn,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{34:$Vn,39:[1,126]},{15:76,16:$Vf,17:$Vg,27:127,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,34:$Vn,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:128,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,34:$Vn,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:69,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,35:129,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:96,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,39:[1,130],40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},o($Ve,[2,59]),o($Ve,[2,61]),o($Ve,[2,64]),o($Ve,[2,65]),{15:76,16:$Vf,17:$Vg,27:131,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:69,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,35:132,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:69,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,34:[1,134],35:133,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:96,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,39:[1,135],40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:136,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,34:$Vn,36:17,37:$V1,38:$V2,39:[1,137],40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},o($Ve,[2,40]),{34:$Vn,39:[1,138]},{34:$Vn,39:[1,139]},{15:76,16:$Vf,17:$Vg,27:96,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,39:[1,140],40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},o($Ve,[2,58]),{34:$Vn,39:[1,141]},{15:76,16:$Vf,17:$Vg,27:96,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,39:[1,142],40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:96,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,39:[1,143],40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:69,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,35:144,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:69,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,34:[1,146],35:145,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{34:$Vn,39:[1,147]},o($Ve,[2,34]),o($Ve,[2,41]),o($Ve,[2,32]),o($Ve,[2,57]),o($Ve,[2,42]),o($Ve,[2,43]),o($Ve,[2,35]),{15:76,16:$Vf,17:$Vg,27:96,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,39:[1,148],40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:96,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,39:[1,149],40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},{15:76,16:$Vf,17:$Vg,27:69,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,35:150,36:17,37:$V1,38:$V2,40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},o($Ve,[2,33]),o($Ve,[2,36]),o($Ve,[2,37]),{15:76,16:$Vf,17:$Vg,27:96,28:$Vh,29:$Vi,30:72,31:73,32:$Vj,33:$Vk,36:17,37:$V1,38:$V2,39:[1,151],40:15,41:$V3,42:14,43:$V4,44:$V5,45:16,46:$V6,47:13,48:$V7,50:8,51:9,53:10,54:$V8,55:$V9,56:11,57:$Va,58:12,59:$Vb},o($Ve,[2,38])],
defaultActions: {29:[2,77]},
parseError: function parseError(str, hash) {
    if (hash.recoverable) {
        this.trace(str);
    } else {
        function _parseError (msg, hash) {
            this.message = msg;
            this.hash = hash;
        }
        _parseError.prototype = Error;

        throw new _parseError(str, hash);
    }
},
parse: function parse(input) {
    var self = this, stack = [0], tstack = [], vstack = [null], lstack = [], table = this.table, yytext = '', yylineno = 0, yyleng = 0, recovering = 0, TERROR = 2, EOF = 1;
    var args = lstack.slice.call(arguments, 1);
    var lexer = Object.create(this.lexer);
    var sharedState = { yy: {} };
    for (var k in this.yy) {
        if (Object.prototype.hasOwnProperty.call(this.yy, k)) {
            sharedState.yy[k] = this.yy[k];
        }
    }
    lexer.setInput(input, sharedState.yy);
    sharedState.yy.lexer = lexer;
    sharedState.yy.parser = this;
    if (typeof lexer.yylloc == 'undefined') {
        lexer.yylloc = {};
    }
    var yyloc = lexer.yylloc;
    lstack.push(yyloc);
    var ranges = lexer.options && lexer.options.ranges;
    if (typeof sharedState.yy.parseError === 'function') {
        this.parseError = sharedState.yy.parseError;
    } else {
        this.parseError = Object.getPrototypeOf(this).parseError;
    }
    function popStack(n) {
        stack.length = stack.length - 2 * n;
        vstack.length = vstack.length - n;
        lstack.length = lstack.length - n;
    }
    _token_stack:
        var lex = function () {
            var token;
            token = lexer.lex() || EOF;
            if (typeof token !== 'number') {
                token = self.symbols_[token] || token;
            }
            return token;
        };
    var symbol, preErrorSymbol, state, action, a, r, yyval = {}, p, len, newState, expected;
    while (true) {
        state = stack[stack.length - 1];
        if (this.defaultActions[state]) {
            action = this.defaultActions[state];
        } else {
            if (symbol === null || typeof symbol == 'undefined') {
                symbol = lex();
            }
            action = table[state] && table[state][symbol];
        }
                    if (typeof action === 'undefined' || !action.length || !action[0]) {
                var errStr = '';
                expected = [];
                for (p in table[state]) {
                    if (this.terminals_[p] && p > TERROR) {
                        expected.push('\'' + this.terminals_[p] + '\'');
                    }
                }
                if (lexer.showPosition) {
                    errStr = 'Parse error on line ' + (yylineno + 1) + ':\n' + lexer.showPosition() + '\nExpecting ' + expected.join(', ') + ', got \'' + (this.terminals_[symbol] || symbol) + '\'';
                } else {
                    errStr = 'Parse error on line ' + (yylineno + 1) + ': Unexpected ' + (symbol == EOF ? 'end of input' : '\'' + (this.terminals_[symbol] || symbol) + '\'');
                }
                this.parseError(errStr, {
                    text: lexer.match,
                    token: this.terminals_[symbol] || symbol,
                    line: lexer.yylineno,
                    loc: yyloc,
                    expected: expected
                });
            }
        if (action[0] instanceof Array && action.length > 1) {
            throw new Error('Parse Error: multiple actions possible at state: ' + state + ', token: ' + symbol);
        }
        switch (action[0]) {
        case 1:
            stack.push(symbol);
            vstack.push(lexer.yytext);
            lstack.push(lexer.yylloc);
            stack.push(action[1]);
            symbol = null;
            if (!preErrorSymbol) {
                yyleng = lexer.yyleng;
                yytext = lexer.yytext;
                yylineno = lexer.yylineno;
                yyloc = lexer.yylloc;
                if (recovering > 0) {
                    recovering--;
                }
            } else {
                symbol = preErrorSymbol;
                preErrorSymbol = null;
            }
            break;
        case 2:
            len = this.productions_[action[1]][1];
            yyval.$ = vstack[vstack.length - len];
            yyval._$ = {
                first_line: lstack[lstack.length - (len || 1)].first_line,
                last_line: lstack[lstack.length - 1].last_line,
                first_column: lstack[lstack.length - (len || 1)].first_column,
                last_column: lstack[lstack.length - 1].last_column
            };
            if (ranges) {
                yyval._$.range = [
                    lstack[lstack.length - (len || 1)].range[0],
                    lstack[lstack.length - 1].range[1]
                ];
            }
            r = this.performAction.apply(yyval, [
                yytext,
                yyleng,
                yylineno,
                sharedState.yy,
                action[1],
                vstack,
                lstack
            ].concat(args));
            if (typeof r !== 'undefined') {
                return r;
            }
            if (len) {
                stack = stack.slice(0, -1 * len * 2);
                vstack = vstack.slice(0, -1 * len);
                lstack = lstack.slice(0, -1 * len);
            }
            stack.push(this.productions_[action[1]][0]);
            vstack.push(yyval.$);
            lstack.push(yyval._$);
            newState = table[stack[stack.length - 2]][stack[stack.length - 1]];
            stack.push(newState);
            break;
        case 3:
            return true;
        }
    }
    return true;
}};

  const translator = require('./translator.js');
/* generated by jison-lex 0.3.4 */
var lexer = (function(){
var lexer = ({

EOF:1,

parseError:function parseError(str, hash) {
        if (this.yy.parser) {
            this.yy.parser.parseError(str, hash);
        } else {
            throw new Error(str);
        }
    },

// resets the lexer, sets new input
setInput:function (input, yy) {
        this.yy = yy || this.yy || {};
        this._input = input;
        this._more = this._backtrack = this.done = false;
        this.yylineno = this.yyleng = 0;
        this.yytext = this.matched = this.match = '';
        this.conditionStack = ['INITIAL'];
        this.yylloc = {
            first_line: 1,
            first_column: 0,
            last_line: 1,
            last_column: 0
        };
        if (this.options.ranges) {
            this.yylloc.range = [0,0];
        }
        this.offset = 0;
        return this;
    },

// consumes and returns one char from the input
input:function () {
        var ch = this._input[0];
        this.yytext += ch;
        this.yyleng++;
        this.offset++;
        this.match += ch;
        this.matched += ch;
        var lines = ch.match(/(?:\r\n?|\n).*/g);
        if (lines) {
            this.yylineno++;
            this.yylloc.last_line++;
        } else {
            this.yylloc.last_column++;
        }
        if (this.options.ranges) {
            this.yylloc.range[1]++;
        }

        this._input = this._input.slice(1);
        return ch;
    },

// unshifts one char (or a string) into the input
unput:function (ch) {
        var len = ch.length;
        var lines = ch.split(/(?:\r\n?|\n)/g);

        this._input = ch + this._input;
        this.yytext = this.yytext.substr(0, this.yytext.length - len);
        //this.yyleng -= len;
        this.offset -= len;
        var oldLines = this.match.split(/(?:\r\n?|\n)/g);
        this.match = this.match.substr(0, this.match.length - 1);
        this.matched = this.matched.substr(0, this.matched.length - 1);

        if (lines.length - 1) {
            this.yylineno -= lines.length - 1;
        }
        var r = this.yylloc.range;

        this.yylloc = {
            first_line: this.yylloc.first_line,
            last_line: this.yylineno + 1,
            first_column: this.yylloc.first_column,
            last_column: lines ?
                (lines.length === oldLines.length ? this.yylloc.first_column : 0)
                 + oldLines[oldLines.length - lines.length].length - lines[0].length :
              this.yylloc.first_column - len
        };

        if (this.options.ranges) {
            this.yylloc.range = [r[0], r[0] + this.yyleng - len];
        }
        this.yyleng = this.yytext.length;
        return this;
    },

// When called from action, caches matched text and appends it on next action
more:function () {
        this._more = true;
        return this;
    },

// When called from action, signals the lexer that this rule fails to match the input, so the next matching rule (regex) should be tested instead.
reject:function () {
        if (this.options.backtrack_lexer) {
            this._backtrack = true;
        } else {
            return this.parseError('Lexical error on line ' + (this.yylineno + 1) + '. You can only invoke reject() in the lexer when the lexer is of the backtracking persuasion (options.backtrack_lexer = true).\n' + this.showPosition(), {
                text: "",
                token: null,
                line: this.yylineno
            });

        }
        return this;
    },

// retain first n characters of the match
less:function (n) {
        this.unput(this.match.slice(n));
    },

// displays already matched input, i.e. for error messages
pastInput:function () {
        var past = this.matched.substr(0, this.matched.length - this.match.length);
        return (past.length > 20 ? '...':'') + past.substr(-20).replace(/\n/g, "");
    },

// displays upcoming input, i.e. for error messages
upcomingInput:function () {
        var next = this.match;
        if (next.length < 20) {
            next += this._input.substr(0, 20-next.length);
        }
        return (next.substr(0,20) + (next.length > 20 ? '...' : '')).replace(/\n/g, "");
    },

// displays the character position where the lexing error occurred, i.e. for error messages
showPosition:function () {
        var pre = this.pastInput();
        var c = new Array(pre.length + 1).join("-");
        return pre + this.upcomingInput() + "\n" + c + "^";
    },

// test the lexed token: return FALSE when not a match, otherwise return token
test_match:function (match, indexed_rule) {
        var token,
            lines,
            backup;

        if (this.options.backtrack_lexer) {
            // save context
            backup = {
                yylineno: this.yylineno,
                yylloc: {
                    first_line: this.yylloc.first_line,
                    last_line: this.last_line,
                    first_column: this.yylloc.first_column,
                    last_column: this.yylloc.last_column
                },
                yytext: this.yytext,
                match: this.match,
                matches: this.matches,
                matched: this.matched,
                yyleng: this.yyleng,
                offset: this.offset,
                _more: this._more,
                _input: this._input,
                yy: this.yy,
                conditionStack: this.conditionStack.slice(0),
                done: this.done
            };
            if (this.options.ranges) {
                backup.yylloc.range = this.yylloc.range.slice(0);
            }
        }

        lines = match[0].match(/(?:\r\n?|\n).*/g);
        if (lines) {
            this.yylineno += lines.length;
        }
        this.yylloc = {
            first_line: this.yylloc.last_line,
            last_line: this.yylineno + 1,
            first_column: this.yylloc.last_column,
            last_column: lines ?
                         lines[lines.length - 1].length - lines[lines.length - 1].match(/\r?\n?/)[0].length :
                         this.yylloc.last_column + match[0].length
        };
        this.yytext += match[0];
        this.match += match[0];
        this.matches = match;
        this.yyleng = this.yytext.length;
        if (this.options.ranges) {
            this.yylloc.range = [this.offset, this.offset += this.yyleng];
        }
        this._more = false;
        this._backtrack = false;
        this._input = this._input.slice(match[0].length);
        this.matched += match[0];
        token = this.performAction.call(this, this.yy, this, indexed_rule, this.conditionStack[this.conditionStack.length - 1]);
        if (this.done && this._input) {
            this.done = false;
        }
        if (token) {
            return token;
        } else if (this._backtrack) {
            // recover context
            for (var k in backup) {
                this[k] = backup[k];
            }
            return false; // rule action called reject() implying the next rule should be tested instead.
        }
        return false;
    },

// return next match in input
next:function () {
        if (this.done) {
            return this.EOF;
        }
        if (!this._input) {
            this.done = true;
        }

        var token,
            match,
            tempMatch,
            index;
        if (!this._more) {
            this.yytext = '';
            this.match = '';
        }
        var rules = this._currentRules();
        for (var i = 0; i < rules.length; i++) {
            tempMatch = this._input.match(this.rules[rules[i]]);
            if (tempMatch && (!match || tempMatch[0].length > match[0].length)) {
                match = tempMatch;
                index = i;
                if (this.options.backtrack_lexer) {
                    token = this.test_match(tempMatch, rules[i]);
                    if (token !== false) {
                        return token;
                    } else if (this._backtrack) {
                        match = false;
                        continue; // rule action called reject() implying a rule MISmatch.
                    } else {
                        // else: this is a lexer rule which consumes input without producing a token (e.g. whitespace)
                        return false;
                    }
                } else if (!this.options.flex) {
                    break;
                }
            }
        }
        if (match) {
            token = this.test_match(match, rules[index]);
            if (token !== false) {
                return token;
            }
            // else: this is a lexer rule which consumes input without producing a token (e.g. whitespace)
            return false;
        }
        if (this._input === "") {
            return this.EOF;
        } else {
            return this.parseError('Lexical error on line ' + (this.yylineno + 1) + '. Unrecognized text.\n' + this.showPosition(), {
                text: "",
                token: null,
                line: this.yylineno
            });
        }
    },

// return next match that has a token
lex:function lex() {
        var r = this.next();
        if (r) {
            return r;
        } else {
            return this.lex();
        }
    },

// activates a new lexer condition state (pushes the new lexer condition state onto the condition stack)
begin:function begin(condition) {
        this.conditionStack.push(condition);
    },

// pop the previously active lexer condition state off the condition stack
popState:function popState() {
        var n = this.conditionStack.length - 1;
        if (n > 0) {
            return this.conditionStack.pop();
        } else {
            return this.conditionStack[0];
        }
    },

// produce the lexer rule set which is active for the currently active lexer condition state
_currentRules:function _currentRules() {
        if (this.conditionStack.length && this.conditionStack[this.conditionStack.length - 1]) {
            return this.conditions[this.conditionStack[this.conditionStack.length - 1]].rules;
        } else {
            return this.conditions["INITIAL"].rules;
        }
    },

// return the currently active lexer condition state; when an index argument is provided it produces the N-th previous condition state, if available
topState:function topState(n) {
        n = this.conditionStack.length - 1 - Math.abs(n || 0);
        if (n >= 0) {
            return this.conditionStack[n];
        } else {
            return "INITIAL";
        }
    },

// alias for begin(condition)
pushState:function pushState(condition) {
        this.begin(condition);
    },

// return the number of states currently on the stack
stateStackSize:function stateStackSize() {
        return this.conditionStack.length;
    },
options: {},
performAction: function anonymous(yy,yy_,$avoiding_name_collisions,YY_START) {
var YYSTATE=YY_START;
switch($avoiding_name_collisions) {
case 0:return 64
break;
case 1:return 32
break;
case 2:return 38
break;
case 3:return 39
break;
case 4:return 4
break;
case 5:return 5
break;
case 6:return 7
break;
case 7:return 6
break;
case 8:return 9
break;
case 9:return 8
break;
case 10:return '>='
break;
case 11:return '<='
break;
case 12:return 10
break;
case 13:return 55
break;
case 14:return 16
break;
case 15:return 17
break;
case 16:return 48
break;
case 17:return 23
break;
case 18:return 24
break;
case 19:return 54
break;
case 20:return 41
break;
case 21:return 43
break;
case 22:return 46
break;
case 23:return 'action'
break;
case 24:return 19
break;
case 25:return 21
break;
case 26:return 20
break;
case 27:return 22
break;
case 28:return 59
break;
case 29:return 25
break;
case 30:return 26
break;
case 31:return 52
break;
case 32:return 11
break;
case 33:return 12
break;
case 34:return 13
break;
case 35:return 37
break;
case 36:return 14
break;
case 37:return 34
break;
case 38:return 28
break;
case 39:return 29
break;
case 40:return 61
break;
case 41:return 33
break;
}
},
rules: [/^(?:$)/,/^(?:[0-9]+(\.[0-9]+)?\b)/,/^(?:\()/,/^(?:\))/,/^(?:\+)/,/^(?:-)/,/^(?:\*)/,/^(?:\/)/,/^(?:<)/,/^(?:>)/,/^(?:>=)/,/^(?:<=)/,/^(?:=)/,/^(?:`)/,/^(?:true\b)/,/^(?:false\b)/,/^(?:let\b)/,/^(?:push\b)/,/^(?:pop\b)/,/^(?:list\b)/,/^(?:if\b)/,/^(?:local\b)/,/^(?:set!)/,/^(?:action\b)/,/^(?:reduce\b)/,/^(?:map\b)/,/^(?:nth\b)/,/^(?:each\b)/,/^(?:print\b)/,/^(?:shift\b)/,/^(?:unshift\b)/,/^(?:lambda\b)/,/^(?:or\b)/,/^(?:and\b)/,/^(?:not\b)/,/^(?:untill\b)/,/^(?:\|>)/,/^(?:\s+)/,/^(?:"[^\"\n]*")/,/^(?:"[^\"\n]{1}")/,/^(?:;[^\n]*)/,/^(?:[a-zA-Z][a-zA-Z0-9?]*)/],
conditions: {"INITIAL":{"rules":[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41],"inclusive":true}}
});
return lexer;
})();
parser.lexer = lexer;
function Parser () {
  this.yy = {};
}
Parser.prototype = parser;parser.Parser = Parser;
return new Parser;
})();


if (typeof require !== 'undefined' && typeof exports !== 'undefined') {
exports.parser = parser;
exports.Parser = parser.Parser;
exports.parse = function () { return parser.parse.apply(parser, arguments); };
exports.main = function commonjsMain(args) {
    if (!args[1]) {
        console.log('Usage: '+args[0]+' FILE');
        process.exit(1);
    }
    var source = require('fs').readFileSync(require('path').normalize(args[1]), "utf8");
    return exports.parser.parse(source);
};
if (typeof module !== 'undefined' && require.main === module) {
  exports.main(process.argv.slice(1));
}
}
'use strict';

var i = require('../../lib/interpreter.js');

describe('while', function () {
  it('should work with simple condistion', function () {
    var result = i.exec('\n                          (def n 0)\n                          (loop (< n 10) (set! n (+ n 1)))\n                          (n)\n                          ');
    expect(result).toBe(10);
  });
});
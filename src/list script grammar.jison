/* lexical grammar */
%{
  const translator = require('./translator.js');
%}

%lex
%%
<<EOF>>                         return 'end_of_file'
[0-9]+("."[0-9]+)?\b            return 'number'
'('                             return '('
')'                             return ')'
'+'                             return '+'
'-'                             return '-'
'*'                             return '*'
'/'                             return '/'
'<'                             return '<'
'>'                             return '>'
'>='                            return '>='
'<='                            return '<='
'='                             return '='
'`'                             return '`'
'true'                          return 'true'
'false'                         return 'false'
'let'                           return 'let'
'push'                          return 'push'
'pop'                           return 'pop'
'list'                          return 'list'
'if'                            return 'if'
'local'                         return 'local'
'set!'                          return 'set!'
'action'                        return 'action'
'reduce'                        return 'reduce'
'map'                           return 'map'
'nth'                           return 'nth'
'each'                          return 'each'
'print'                         return 'print'
'shift'                         return 'shift'
'unshift'                       return 'unshift'
'lambda'                        return 'fn'
'or'                            return 'or'
'and'                           return 'and'
'not'                           return 'not'
'untill'                        return 'loop'
'|>'                            return 'length'
\s+                             return 'space'
\"[^\"\n]*\"                    return 'string'
\"[^\"\n]{1}\"                  return 'char'
\;[^\n]*&                       return 'comment'
[a-zA-Z][a-zA-Z0-9?]*           return 'name'

/lex

%start file

%% /* language grammar */

operators
  : '+'
    { $$ = translator.add }
  | '-'
    { $$ = translator.substract }
  | '/'
    { $$ = translator.devide }
  | '*'
    { $$ = translator.multiply }
  | '>'
    { $$ = translator.greater }
  | '<'
    { $$ = translator.less }
  | '='
    { $$ = translator.equal }
  | 'or'
    { $$ = translator.or}
  | 'and'
    { $$ = translator.and}
  | 'not'
    { $$ = translator.not }
  | 'length'
    {$$ = translator.length_of}
  ;

bool
: true
| false
;

list_func
: reduce
    { $$ = translator.reduce }
| nth
      { $$ = translator.nth }
| map
      { $$ = translator.map }
| each
    { $$ = translator.each }
|push
    { $$ = translator.conj }
|pop
    { $$ = translator.pop }
|shift
    { $$ = translator.shift }
|unshift
    { $$ = translator.unshift }

;

value
  : string
  | char
  | expr
  | statement
  | number
    { $$ = +$1 }
  | name
  | bool
  | value space
  ;

values
  : value
    { $$ = [$1] }
  | values value
    { $$ = translator.collectArgs($1, $2)}
  ;

loop_statement
  :  loop '(' space value value')'
    {$$ = { type: 'while', pred: $4, body: $5 }}
  ;

if_statement
  :  if '(' space value value value ')'
    { $$ = { type: 'if', cond: $4, true: $5, false: $6} }
  |  if'(' space value value ')'
    { $$ = { type: 'if', cond: $4, true: $5 } }
  ;

let_statement
  : local '(' '(' values ')' values'')'
    { $$ = { expr: $4, type: 'let', values: $6 }}
  | local '(' '(' values ')' space values ')'
    { $$ = { expr: $4, type: 'let', values: $7 }}
  | local '(' space '(' values ')' values ')'
    { $$ = { expr: $5, type: 'let', values: $7 }}
  | local '(' space '(' values ')' space values ')'
    { $$ = { expr: $5, type: 'let', values: $8 }}
  | list_statement
  ;

set_statement
  :  'set!' '(' value value ')'
    { $$ = { expr: $3, type: 'set', values: [$4]  } }
  |  'set!' '(' space  value value ')'
    { $$ = { expr: $4, type: 'set', values: [$5]  } }
  ;



define_statement
  : let '(' space name space value')'
    { $$ = { expr: $4, type: 'var', values: [$6] }}
  | let '(' space expr space values')'
    { $$ = { expr: $4, type: 'function', values: $6 }; }
  ;

statement
  : define_statement
  | let_statement
  | if_statement
  | set_statement
  | loop_statement
  ;

id
  : name
  | list_func
  | operators
  | space name
  | id space
  ;

simple_expr
  : '(' id ')'
    { $$ =  { id: $2, values: [] }}
  | '(' ')'
    { $$ =  { values: [] }}
  | '(' id values ')'
    { $$ =  { id: $2, values: $3 }}
  ;

fn_expr
  :'(' fn space expr space values')'
    { $$ = { expr: $4, type: 'fn', values: $6 }}
  |'(' fn expr space values')'
    { $$ = { expr: $3, type: 'fn', values: $5 }}
  |'(' fn expr values')'
    { $$ = { expr: $3, type: 'fn', values: $4 }}
  ;

list_expr
  :  list '(' values ')'
    { $$ = `Array(${$3.map(translator.parse)})` }
  |  list  '(' space values ')'
    { $$ = `Array(${$4.map(translator.parse)})` }
  |'`' '(' values ')'
    { $$ = `Array(${$3.map(translator.parse)})` }
  ;

action_expr
  :  action '(' values')'
    { $$ = { type: 'do', values: $3 }}
  |   '(' space values')'
    { $$ = { type: 'do', values: $4 }}
  ;

print_expr
  :  print '(' space value ')'
    { $$ = { type: 'print', value: $4 } }
  ;

expr
  : simple_expr
  | fn_expr
  | list_expr
  | _expr
  | print_expr
  ;

code
  : expr
    { $$ = translator.parse($1)}
  | statement
    { $$ = translator.parse($1) }
  | space
  | comment
    { $$ = '' }
  ;

program
  : code
  | program code
    { $$ = $1 + $2 }
  ;

file
  : program end_of_file
    { return $1 }
  ;
